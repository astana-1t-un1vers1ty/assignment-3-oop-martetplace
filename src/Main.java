import kz.aitu.oop.practice.marketplace.config.ConfigLoader;
import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.UserRepository;
import kz.aitu.oop.practice.marketplace.menus.MainMenu;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.migrator.Migrations;

public class Main {
    public static void main(String[] args) {
        ConfigLoader.load();
        new Migrations(JDBCConnection.instance.connection).migrate();
        var user = UserRepository.instance.getAuthorized();
        new MainMenu(user != null ? user : User.Guest()).run(null);
    }
}