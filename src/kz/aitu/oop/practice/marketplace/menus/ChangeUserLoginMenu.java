package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.UserRepository;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

import java.util.Scanner;

public class ChangeUserLoginMenu extends Menu {
    ChangeUserLoginMenu(Menu parent, User user){
        super("Change user login", parent, user, new MenuOptions().UseBackButton());
    }

    @Override
    public void run(Menu parent){
        Scanner scanner=new Scanner(System.in);
        System.out.print("Enter new Login: ");
        String login = scanner.nextLine();
        if (UserRepository.instance.getByLogin(login)==null){
            getUser().setLogin(login);
            UserRepository.instance.update(getUser());
            UserRepository.instance.authorize(getUser());
            System.out.println("Login has been successfully changed!");
        }
        else{
            System.out.println("User with this login already exists! Please choose another. ");
        }
        this.back();
    }
}
