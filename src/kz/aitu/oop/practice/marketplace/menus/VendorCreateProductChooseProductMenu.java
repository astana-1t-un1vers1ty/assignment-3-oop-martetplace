package kz.aitu.oop.practice.marketplace.menus;
import kz.aitu.oop.practice.marketplace.db.repositories.VendorProductRepository;
import kz.aitu.oop.practice.marketplace.models.Product;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.models.VendorProduct;
import kz.aitu.oop.practice.marketplace.models.VendorUser;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;

import java.util.Scanner;

public class VendorCreateProductChooseProductMenu extends Menu {
    private final Product product;
    public VendorCreateProductChooseProductMenu(Menu parent, Product product, User user) {
        super(product.getName(), parent, user);
        this.product = product;
    }
    @Override
    public void run(Menu parent){
        var scanner = new Scanner(System.in);
        System.out.print("Enter price: ");
        var price = scanner.nextDouble();
        var user = (VendorUser)getUser();
        VendorProductRepository.instance.save(new VendorProduct(user.getVendor(), product, price));
        System.out.println("The product was added.");
        new MainMenu(getUser()).run(null);
    }
}
