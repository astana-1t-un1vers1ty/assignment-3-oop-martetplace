package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.OrdersRepository;
import kz.aitu.oop.practice.marketplace.db.repositories.VendorProductRepository;
import kz.aitu.oop.practice.marketplace.models.Order;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

import java.util.Collection;

public class ViewCompletedOrders extends Menu {
    public ViewCompletedOrders(Menu parent, User user){
        super("View completed orders", parent, user, new MenuOptions().UseBackButton());
    }

    @Override
    public void run(Menu parent){
        Collection<Order> orders = OrdersRepository.instance.getAllByVendorId(getUser().getVendorId());
        for(Order order:orders){
            if(order.isCompleted()){
                System.out.println("Product name: " + order.getProduct().getName());
                System.out.println("Price: " + VendorProductRepository.instance.
                        getPriceByVendorAndProductId(getUser().getVendorId(),order.getProduct().getId()));
                System.out.println("Address: " + order.getAddress());
                this.printDelimiter(20);
            }
        }
        this.back();
    }
}
