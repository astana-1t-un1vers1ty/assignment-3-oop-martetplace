package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.CountryRepository;
import kz.aitu.oop.practice.marketplace.models.Product;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.models.Vendor;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;

public class BuySelectCountryMenu extends Menu {
    private final Product product;
    private final Vendor vendor;
    public BuySelectCountryMenu(String title, Menu parent, Product product, Vendor vendor, User user) {
        super(title, parent, user);
        this.vendor = vendor;
        this.product = product;
    }

    @Override
    public void run(Menu parent) {
        var countries = CountryRepository.instance.getAll();
        for (var country : countries) {
            super.addItem(new BuySelectRegionMenu(country.getName(), this, product, vendor, country, getUser()));
        }
        super.run(parent);
    }
}
