package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.UserRepository;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

import java.util.Scanner;

public class ChangeUserNameMenu extends Menu {
    ChangeUserNameMenu(Menu parent, User user){
        super("Change user name", parent, user, new MenuOptions().UseBackButton());
    }

    @Override
    public void run(Menu parent){
        Scanner scanner=new Scanner(System.in);
        System.out.print("Enter new Full name: ");
        String fullName = scanner.nextLine();
        getUser().setFullName(fullName);
        UserRepository.instance.update(getUser());
        System.out.println("Full name has been successfully changed!");
        this.back();
    }
}

