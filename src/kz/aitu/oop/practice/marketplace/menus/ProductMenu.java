package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.VendorProductRepository;
import kz.aitu.oop.practice.marketplace.models.Product;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.models.VendorProduct;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuItem;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

import java.util.Map;
import java.util.Scanner;

public class ProductMenu extends Menu {
    private final Product product;
    private final VendorProduct[] vendorProducts;
    public ProductMenu(Menu parent, Product product, User user) {
        super(product.getName(), parent, user, new MenuOptions().UseBackButton());
        this.product = product;
        this.vendorProducts = VendorProductRepository.instance.getByProductId(product.id);
        super.addItem(new BuySelectVendorMenu(vendorProducts, this, getUser()));
    }

    @Override
    public void display() {
        printTitle();
        printBackButton();
        System.out.println(product.getBrand().getName());
        System.out.println(product.getCategory().getName());
        for (int i = 0; i < super.getItems().size(); i++) {
            if (super.getItems().get(i) instanceof MenuItem item) {
                System.out.println((i + 1) + ". " + item.getTitle());
            }
            if (super.getItems().get(i) instanceof Menu item) {
                System.out.println((i + 1) + ". " + item.getTitle());
            }
        }
        var delimiterCount = 60;
        printDelimiter("#", delimiterCount);
        for (Map.Entry<String, Object> entry : product.getCharacteristics().entrySet()) {
            var key = entry.getKey();
            var value = entry.getValue();
            System.out.println(key + " ".repeat(delimiterCount - key.length() - value.toString().length()) + entry.getValue());
        }
        printDelimiter("#", delimiterCount);
        for (var vendor: vendorProducts) {
            var vName = vendor.getVendor().getName().replace("\n", "");
            var price = vendor.getPrice();
            System.out.println(vName + " ".repeat(delimiterCount - vName.length() - String.valueOf(price).length()) + price);
        }
    }

    @Override
    public void run(Menu parent) {
        Scanner input = new Scanner(System.in);
        display();
        System.out.print("Enter your choice (number): ");
        int choice = input.nextInt();
        checkChoice(choice);
    }
}
