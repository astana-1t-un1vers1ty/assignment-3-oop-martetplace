package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class AccountMenu extends Menu {
    AccountMenu(Menu parent, User user){
        super("Account menu", parent, user, new MenuOptions().UseBackButton());
        super.addItem(new AccountInformationMenu(this, user));
        super.addItem(new AccountEditMenu(this, user));
    }

}
