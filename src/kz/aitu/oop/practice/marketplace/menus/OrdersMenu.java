package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class OrdersMenu extends Menu {

    public OrdersMenu(Menu parent, User user) {
        super("Orders", parent, user, new MenuOptions().UseBackButton());
        super.addItem(new ViewOrdersMenu(this, user));
        super.addItem(new ViewCompletedOrders(this, user));
        super.addItem(new CompleteOrderMenu(this, user));
    }
}
