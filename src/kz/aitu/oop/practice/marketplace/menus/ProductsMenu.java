package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.models.Product;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class ProductsMenu extends Menu {
    public ProductsMenu(Menu parent, Product[] products, User user, boolean useCategoryButton) {
        super("Products", parent, user, new MenuOptions().UseBackButton().setDelimiterAfter(2));
        if (super.getItems().size() == 0) {
            if (useCategoryButton) {
                super.addItem(new CategoryMenu(this, products, getUser()));
            } else {
                super.addItem(new CharacteristicMenu(this, products, user));
            }
            for (var product: products) {
                super.addItem(new ProductMenu(this, product, getUser()));
            }
        }
    }
}
