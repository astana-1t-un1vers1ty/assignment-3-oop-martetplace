package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.UserRepository;
import kz.aitu.oop.practice.marketplace.db.repositories.VendorRepository;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.models.Vendor;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;
import kz.aitu.oop.practice.marketplace.refs.Role;

public class AccountInformationMenu extends Menu{
    AccountInformationMenu(Menu parent, User user){
        super("Account Information", parent, user, new MenuOptions().UseBackButton());

    }

    @Override
    public void run(Menu parent){
        switch (getUser().getRole()){
            case User:
                System.out.println("Full Name: "+  getUser().getFullName());
                System.out.println("Login : "+  getUser().getLogin());
                break;
            case Vendor:
                System.out.println("Full Name: "+  getUser().getFullName());
                System.out.println("Vendor name: "+ VendorRepository.instance.get(getUser().getVendorId()).getName());
                break;
        }
        this.back();
    }
}
