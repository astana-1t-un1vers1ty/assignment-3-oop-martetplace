package kz.aitu.oop.practice.marketplace.menus;
import kz.aitu.oop.practice.marketplace.db.repositories.CategoryRepository;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class VendorCreateProductMenu extends Menu {

    public VendorCreateProductMenu(Menu parent, User user) {
        super("Create product", parent, user, new MenuOptions().UseBackButton());
    }

    @Override
    public void run(Menu parent){
        var categories = CategoryRepository.instance.getByParent(1);
        for (var c : categories){
            super.addItem(new VendorCreateProductChooseCategoryMenu(c, this, getUser()));
        }
        super.run(parent);
    }
}
