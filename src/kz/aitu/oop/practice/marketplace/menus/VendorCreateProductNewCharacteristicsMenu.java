package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.models.Category;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;

import java.util.Map;
import java.util.Scanner;

public class VendorCreateProductNewCharacteristicsMenu extends Menu {
    private final Category category;
    private final Map<String, Object> characteristics;
    public VendorCreateProductNewCharacteristicsMenu(Category category, Menu parent, User user, Map<String, Object> characteristics) {
        super("", parent, user);
        this.characteristics = characteristics;
        this.category = category;
    }
    @Override
    public void run(Menu parent){
        var sc = new Scanner(System.in);
        System.out.print(": ");
        var name = sc.nextLine();
    }
}
