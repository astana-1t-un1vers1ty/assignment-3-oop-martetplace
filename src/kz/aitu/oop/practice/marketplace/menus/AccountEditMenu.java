package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class AccountEditMenu extends Menu {
    AccountEditMenu(Menu parent, User user){
        super("Edit account information", parent, user, new MenuOptions().UseBackButton());
        super.addItem(new ChangeUserNameMenu(this, user));
        super.addItem(new ChangeUserLoginMenu(this, user));
        super.addItem(new ChangeUserPasswordMenu(this, user));
    }
}
