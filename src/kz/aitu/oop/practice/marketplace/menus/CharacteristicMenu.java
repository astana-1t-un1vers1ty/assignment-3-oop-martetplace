package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.helpers.CharacteristicHelper;
import kz.aitu.oop.practice.marketplace.models.Product;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuItem;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class CharacteristicMenu extends Menu {
    private final Product[] products;
    public CharacteristicMenu(Menu parent, Product[] products, User user) {
        super("Choose characteristic", parent, user, new MenuOptions().UseBackButton());
        this.products = products;
    }

    @Override
    public void run(Menu parent) {
        var characteristics = new String[0];
        if (products.length > 0) {
            characteristics = CharacteristicHelper.getCharacteristicKeys(products[0].getCategory().id);
        }
        for (var c : characteristics) {
            super.addItem(new MenuItem(c, getUser(), new CharacteristicValuesMenu(this, c, products, getUser())));
        }
        super.run(parent);
    }
}
