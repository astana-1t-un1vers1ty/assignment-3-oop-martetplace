package kz.aitu.oop.practice.marketplace.menus;
import kz.aitu.oop.practice.marketplace.db.repositories.CategoryRepository;
import kz.aitu.oop.practice.marketplace.models.Category;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class VendorCreateProductChooseCategoryMenu extends Menu{
    private final Category[] categories;
    private final Category category;
    public VendorCreateProductChooseCategoryMenu(Category category, Menu parent, User user) {
        super(category.getName(), parent, user, new MenuOptions().UseBackButton());
        categories = CategoryRepository.instance.getByParent(category.id);
        this.category = category;
    }

    @Override
    public void run(Menu parent){
        if (categories.length == 0){
            new VendorCreateProductListProductsMenu(category, this, getUser()).run(this);
        }
        for (var c : categories){
            super.addItem(new VendorCreateProductChooseCategoryMenu(c, this, getUser()));
        }
        super.run(parent);

    }
}
