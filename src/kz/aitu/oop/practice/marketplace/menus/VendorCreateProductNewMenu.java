package kz.aitu.oop.practice.marketplace.menus;
import kz.aitu.oop.practice.marketplace.db.repositories.BrandRepository;
import kz.aitu.oop.practice.marketplace.db.repositories.ProductRepository;
import kz.aitu.oop.practice.marketplace.db.repositories.VendorProductRepository;
import kz.aitu.oop.practice.marketplace.helpers.CharacteristicHelper;
import kz.aitu.oop.practice.marketplace.models.*;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static kz.aitu.oop.practice.marketplace.helpers.CharacteristicHelper.getCharacteristicKeys;

public class VendorCreateProductNewMenu extends Menu {
    private final Category category;

    public VendorCreateProductNewMenu(Category category, Menu parent, User user) {
        super("Create new", parent, user);
        this.category = category;
    }
    @Override
    public void run(Menu parent){
        var sc = new Scanner(System.in);
        System.out.print("Enter name of your product: ");
        var name = sc.nextLine();
        System.out.print("Enter brand of your product: ");
        var brandName = sc.nextLine();
        var brand = BrandRepository.instance.getByName(brandName);
        if (brand == null){
            BrandRepository.instance.save(new Brand(-1, brandName));
            brand = BrandRepository.instance.getByName(brandName);
        }
        var keys = CharacteristicHelper.getCharacteristicKeys(category.id);
        Map<String, Object> characteristics = new HashMap<>();
        for (var key : keys){
            System.out.print("Enter " + key + ": ");
            characteristics.put(key, (Object)sc.nextLine());
        }
        System.out.print("Enter your price: ");
        var price = sc.nextDouble();
        var productId = ProductRepository.instance.save(new Product(-1, name, brand, category, characteristics));
        var product = ProductRepository.instance.getByName(name);
        var user = (VendorUser)getUser();
        VendorProductRepository.instance.save(new VendorProduct(user.getVendor(), product, price));
        System.out.println("The product was added.");
        new MainMenu(getUser()).run(null);
    }
}
