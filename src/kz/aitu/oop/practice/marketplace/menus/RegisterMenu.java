package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.UserRepository;
import kz.aitu.oop.practice.marketplace.helpers.Hasher;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.refs.Role;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class RegisterMenu extends Menu {
    public RegisterMenu(Menu parent, User user) {
        super("Register", parent, user);
    }

    @Override
    public void run(Menu parent){
        var scanner = new Scanner(System.in);
        System.out.print("Enter full name: ");
        var fullName = scanner.nextLine();
        var login = "";
        while (true){
            System.out.print("Enter login: ");
            login = scanner.nextLine();
            if(checkExistingUser(login)) System.out.println("The user with this login already exists! Please try again.");
            else break;
        }

        var password = "";
        do {
            System.out.print("Enter password: ");
            password = scanner.nextLine();
            System.out.println("Password must consist of at least 8 characters.");
        } while (!checkPassword(password));

        User user = new User(0, fullName, Role.User, login, Hasher.bytesToHex(password.getBytes(StandardCharsets.UTF_8)), 0);
        if(UserRepository.instance.save(user)!=null) {
            System.out.println("An account was created!");
        }
        else {
            System.out.println("Could not create an account");
        }
        this.back();


    }

    private boolean checkExistingUser(String login){
        return UserRepository.instance.getByLogin(login)!=null;
    }

    private boolean checkPassword(String password){
        return password.length()>7;
    }

}
