package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class LoginRegisterMenu extends Menu {
    public LoginRegisterMenu(Menu parent, User user) {
        super("Login/Register", parent, user, new MenuOptions().UseBackButton());
        super.addItem(new LoginMenu(this, user));
        super.addItem(new RegisterMenu(this, user));
    }
}
