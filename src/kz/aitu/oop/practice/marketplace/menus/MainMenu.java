package kz.aitu.oop.practice.marketplace.menus;

import com.sun.tools.javac.Main;
import kz.aitu.oop.practice.marketplace.db.repositories.UserRepository;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuItem;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class MainMenu extends Menu {
    public MainMenu(User user) {
        super("Main menu", null, user, new MenuOptions().UseExitButton());
        switch (user.getRole()) {
            case Guest: {
                super.addItem(new LoginRegisterMenu(this, user));
                super.addItem(new SearchForProductMenu(this, user));
                break;
            }
            case User:{
                super.addItem( new AccountMenu(this, user));
                super.addItem(new SearchForProductMenu(this, user));
                super.addItem(new MenuItem("Log out", getUser(), (parent -> {
                    UserRepository.instance.unauthorize(getUser().getId());
                    new MainMenu(User.Guest()).run(null);
                })));
                break;
            }
            case Vendor:{
                super.addItem(new OrdersMenu(this, user));
                super.addItem(new VendorCreateProductMenu(this, user));
                super.addItem(new VendorListAllProducts(this, user));
                super.addItem(new MenuItem("Log out", getUser(), (parent -> {
                    UserRepository.instance.unauthorize(getUser().getId());
                    new MainMenu(User.Guest()).run(null);
                })));
                break;
            }

        }

    }
}
