package kz.aitu.oop.practice.marketplace.menus;
import kz.aitu.oop.practice.marketplace.db.repositories.ProductRepository;
import kz.aitu.oop.practice.marketplace.models.Category;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class VendorCreateProductListProductsMenu extends Menu {
    private final Category category;
    public VendorCreateProductListProductsMenu(Category category, Menu parent, User user) {
        super("", parent, user, new MenuOptions().UseBackButton().setDelimiterAfter(2));
        this.category = category;
    }
    @Override
    public void run(Menu parent){
        var products = ProductRepository.instance.getByCategory(category.id);
        if (super.getItems().size() == 0) {
            super.addItem(new VendorCreateProductNewMenu(category, this, getUser()));
            for (var product: products) {
                super.addItem(new VendorCreateProductChooseProductMenu(this, product, getUser()));
            }
        }
        super.run(parent);
    }
}
