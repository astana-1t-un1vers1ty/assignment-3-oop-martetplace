package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.RegionRepository;
import kz.aitu.oop.practice.marketplace.models.Country;
import kz.aitu.oop.practice.marketplace.models.Product;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.models.Vendor;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class BuySelectRegionMenu extends Menu {
    private final Product product;
    private final Vendor vendor;
    private final Country country;
    public BuySelectRegionMenu(String title, Menu parent, Product product, Vendor vendor, Country country, User user) {
        super(title, parent, user, new MenuOptions().UseBackButton());
        this.product = product;
        this.vendor = vendor;
        this.country = country;
    }

    @Override
    public void run(Menu parent) {
        var regions = RegionRepository.instance.getByCountryId(country.id);
        for (var region : regions) {
            super.addItem(new BuySelectCityMenu(region.getName(), this, product, vendor, country, region, getUser()));
        }
        super.run(parent);
    }
}
