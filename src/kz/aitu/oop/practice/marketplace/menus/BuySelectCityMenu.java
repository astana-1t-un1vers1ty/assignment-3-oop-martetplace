package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.CityRepository;
import kz.aitu.oop.practice.marketplace.models.*;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;

public class BuySelectCityMenu extends Menu {
    private final Product product;
    private final Vendor vendor;
    private final Country country;
    private final Region region;

    public BuySelectCityMenu(String title, Menu parent, Product product, Vendor vendor, Country country, Region region, User user) {
        super(title, parent, user);
        this.product = product;
        this.vendor = vendor;
        this.country = country;
        this.region = region;
    }

    @Override
    public void run(Menu parent) {
        var cities = CityRepository.instance.getByRegionId(region.id);
        for (var city : cities) {
            super.addItem(new BuyEnterAddressMenu(city.getName(), this, product, vendor, country, region, city, getUser()));
        }
        super.run(parent);
    }
}
