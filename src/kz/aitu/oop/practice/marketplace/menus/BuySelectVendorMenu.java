package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.models.VendorProduct;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuItem;

public class BuySelectVendorMenu extends Menu {
    private final VendorProduct[] vendorProducts;
    public BuySelectVendorMenu(VendorProduct[] vendorProducts, Menu parent, User user) {
        super("Buy", parent, user);
        this.vendorProducts = vendorProducts;
    }

    @Override
    public void display() {
        var delimiterCount = 60;
        if (super.getItems().size() <= 1) {
            for (var vendor: vendorProducts) {
                var vName = vendor.getVendor().getName().replace("\n", "");
                var price = vendor.getPrice();
                super.addItem(new BuySelectCountryMenu(vName + " ".repeat(delimiterCount - vName.length() - String.valueOf(price).length()) + price, this, vendor.getProduct(), vendor.getVendor(), getUser()));
            }
        }
        for (int i = 0; i < super.getItems().size(); i++) {
            if (super.getItems().get(i) instanceof MenuItem item) {
                System.out.println((i + 1) + ". " + item.getTitle());
            }
            if (super.getItems().get(i) instanceof Menu item) {
                System.out.println((i + 1) + ". " + item.getTitle());
            }
        }
    }
}
