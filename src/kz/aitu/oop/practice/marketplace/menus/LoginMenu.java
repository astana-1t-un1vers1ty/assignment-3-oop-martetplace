package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.UserRepository;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;

import java.util.Scanner;

public class LoginMenu extends Menu {
    public LoginMenu(Menu parent, User user) {
        super("Login", parent, user);
    }

    @Override
    public void run(Menu parent) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter login: ");
        var login = scanner.nextLine();
        System.out.print("Enter password: ");
        var password = scanner.nextLine();
        User user = UserRepository.instance.getByLoginAndPassword(login, password);
        if(user!=null){
            UserRepository.instance.authorize(user);
            new MainMenu(user).run(null);
        }
        else {
            System.out.println("Incorrect login or password");
            this.back();
        }
    }
}
