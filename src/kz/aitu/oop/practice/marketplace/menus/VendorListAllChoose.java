package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.models.Product;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.models.VendorProduct;
import kz.aitu.oop.practice.marketplace.pkg.menus.IRunnableMenu;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class VendorListAllChoose extends Menu {
    private final VendorProduct product;
    public VendorListAllChoose(Menu parent, VendorProduct product, User user) {
        super(product.getProduct().getName(), parent, user, new MenuOptions().UseBackButton());
        this.product = product;
    }
    @Override
    public void run(Menu parent){
        System.out.println("Name: " + product.getProduct().getName());
        System.out.println("Brand: " + product.getProduct().getBrand().getName());
        System.out.println("Category: " + product.getProduct().getCategory().getName());
        System.out.println("Characteristics: " + product.getProduct().getCharacteristics());
        System.out.println("Price: " + product.getPrice());
        printDelimiter(60);
        if (super.getItems().size() == 0) {
            super.addItem(new VendorEditProductPrice(this, product, getUser()));
            super.addItem(new VendorDeleteProduct(this, product, getUser()));
        }
        super.run(parent);
    }
}
