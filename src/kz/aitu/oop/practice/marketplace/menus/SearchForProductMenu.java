package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.ProductRepository;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;

import java.util.Scanner;

public class SearchForProductMenu extends Menu {
    public SearchForProductMenu(Menu parent, User user) {
        super("Search for product", parent, user);
    }

    @Override
    public void run(Menu parent) {
        System.out.print("Search for ");
        var scanner = new Scanner(System.in);
        var search = scanner.nextLine();
        var products = ProductRepository.instance.getProductsLike(search);
        new ProductsMenu(this.getParent(), products, getUser(), true).run(this.getParent());
    }
}
