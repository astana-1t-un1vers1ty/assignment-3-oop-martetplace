package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.OrdersRepository;
import kz.aitu.oop.practice.marketplace.models.Order;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

import java.util.Scanner;

public class CompleteOrderMenu extends Menu {
    public CompleteOrderMenu(Menu parent, User user) {
        super("Complete order", parent, user, new MenuOptions().UseBackButton());
    }

    @Override
    public void run(Menu parent){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter order id: ");
        int id = scanner.nextInt();
        Order order = OrdersRepository.instance.get(id);
        if(order==null){
            System.out.println("There is no order with entered id!");

        }
        else{
            order.setCompleted(true);
            OrdersRepository.instance.update(order);
            System.out.println("The order is complete!");

        }
        this.back();
    }
}
