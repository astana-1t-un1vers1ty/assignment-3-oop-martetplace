package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.VendorProductRepository;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.models.VendorProduct;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class VendorDeleteProduct extends Menu {
    private final VendorProduct product;
    public VendorDeleteProduct(Menu parent, VendorProduct product, User user) {
        super("Delete product", parent, user, new MenuOptions().UseBackButton());
        this.product = product;
    }
    @Override
    public void run(Menu parent){
        VendorProductRepository.instance.delete(product.getVendor().id, product.getProduct().id);

        System.out.println("The product was succesfully deleted");
        printDelimiter(60);
        new MainMenu(getUser()).run(null);
    }
}
