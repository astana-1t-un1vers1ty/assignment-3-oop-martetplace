package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.models.Category;
import kz.aitu.oop.practice.marketplace.models.Product;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuItem;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CategoryMenu extends Menu {
    private final Product[] products;
    public CategoryMenu(Menu parent, Product[] products, User user) {
        super("Category", parent, user, new MenuOptions().UseBackButton());
        this.products = products;
    }

    private Product[] getProductsByCategory(String category, Product[] products) {
        var productsByCategory = new ArrayList<Product>();
        for (var product : products) {
            if (product.getCategory().getName().equals(category)) {
                productsByCategory.add(product);
            }
        }
        return productsByCategory.toArray(new Product[0]);
    }

    @Override
    public void run(Menu parent) {
        var categories = new HashMap<String, Category>();
        for (var product: products) {
            categories.put(product.getCategory().getName(), product.getCategory());
        }
        for (Map.Entry<String, Category> entry : categories.entrySet()) {
            super.addItem(new MenuItem(entry.getKey(), getUser(), new ProductsMenu(getParent().getParent(), getProductsByCategory(entry.getKey(), products), getUser(), false)));
        }
        super.run(parent);
    }
}
