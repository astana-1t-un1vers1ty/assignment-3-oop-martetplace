package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.UserRepository;
import kz.aitu.oop.practice.marketplace.helpers.Hasher;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ChangeUserPasswordMenu extends Menu {
    ChangeUserPasswordMenu(Menu parent, User user){
        super("Change user password", parent, user, new MenuOptions().UseBackButton());
    }

    @Override
    public void run(Menu parent){
        Scanner scanner=new Scanner(System.in);
        System.out.print("Enter new password: ");
        String password = scanner.nextLine();
        password = Hasher.bytesToHex(password.getBytes(StandardCharsets.UTF_8));
        getUser().setPassword(password);
        UserRepository.instance.update(getUser());
        System.out.println("Password has been successfully changed!");
        this.back();
        }
    }


