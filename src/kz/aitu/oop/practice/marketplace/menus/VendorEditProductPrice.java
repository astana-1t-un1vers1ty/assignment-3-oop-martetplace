package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.VendorProductRepository;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.models.VendorProduct;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

import java.util.Scanner;

public class VendorEditProductPrice extends Menu {
    private final VendorProduct product;
    public VendorEditProductPrice(Menu parent, VendorProduct product, User user) {
        super("Edit product price", parent, user, new MenuOptions().UseBackButton());
        this.product = product;
    }
    @Override
    public void run(Menu parent){
        System.out.println("Current price: " + product.getPrice());
        var scanner = new Scanner(System.in);
        System.out.print("Enter new price: ");
        var price = scanner.nextDouble();
        product.setPrice(price);
        VendorProductRepository.instance.update(product);
        System.out.println("The price was updated.");
        printDelimiter(60);
        back();
    }
}
