package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.helpers.CharacteristicHelper;
import kz.aitu.oop.practice.marketplace.models.Product;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuItem;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

import java.util.ArrayList;

public class CharacteristicValuesMenu extends Menu {
    private final String key;
    private final Product[] products;
    public CharacteristicValuesMenu(Menu parent, String key, Product[] products, User user) {
        super("Choose value", parent, user, new MenuOptions().UseBackButton());
        this.key = key;
        this.products = products;
    }

    @Override
    public void run(Menu parent) {
        var values = CharacteristicHelper.getCharacteristicValues(products[0].getCategory().id, key);
        for (var v : values) {
            super.addItem(new MenuItem(String.valueOf(v), getUser(), new ProductsMenu(new MainMenu(getUser()), getProductsByCharacteristicValue(key, v, products), getUser(), false)));
        }
        super.run(parent);
    }

    private Product[] getProductsByCharacteristicValue(String key, Object value, Product[] products) {
        var productsByCharacteristicValue = new ArrayList<Product>();
        for (var product : products) {
            if (product.getCharacteristics().get(key).equals(value)) {
                productsByCharacteristicValue.add(product);
            }
        }
        return productsByCharacteristicValue.toArray(new Product[0]);
    }
}
