package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.OrdersRepository;
import kz.aitu.oop.practice.marketplace.models.*;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

import java.util.Scanner;

public class BuyEnterAddressMenu extends Menu {
    private final Product product;
    private final Vendor vendor;
    private final Country country;
    private final Region region;
    private final City city;
    public BuyEnterAddressMenu(String title, Menu parent, Product product, Vendor vendor, Country country, Region region, City city, User user) {
        super(title, parent, user, new MenuOptions().UseBackButton());
        this.product = product;
        this.vendor = vendor;
        this.country = country;
        this.region = region;
        this.city = city;
    }

    @Override
    public void run(Menu parent) {
        Scanner input = new Scanner(System.in);
        display();
        System.out.print("Enter your address: ");
        var address = input.nextLine();
        OrdersRepository.instance.save(new Order(-1, product, vendor, String.join(", ", country.getName(), region.getName(), city.getName(), address), false));
        new MainMenu(getUser()).run(null);
    }
}
