package kz.aitu.oop.practice.marketplace.menus;

import kz.aitu.oop.practice.marketplace.db.repositories.VendorProductRepository;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.pkg.menus.Menu;
import kz.aitu.oop.practice.marketplace.pkg.menus.MenuOptions;

public class VendorListAllProducts extends Menu {

    public VendorListAllProducts(Menu parent, User user) {
        super("List all your products", parent, user, new MenuOptions().UseBackButton());
    }
    @Override
    public void run(Menu parent){
        var products = VendorProductRepository.instance.getByVendorId(getUser().getVendorId());
        if (super.getItems().size() == 0) {
            for (var product : products) {
                super.addItem(new VendorListAllChoose(this, product, getUser()));
            }
        }
        super.run(parent);
    }
}
