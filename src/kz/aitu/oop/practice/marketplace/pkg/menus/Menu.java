package kz.aitu.oop.practice.marketplace.pkg.menus;

import kz.aitu.oop.practice.marketplace.models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu implements IRunnableMenu {
    private final String title;
    private final List<IRunnableMenu> items;
    private final Menu parent;
    private User user;
    private MenuOptions options = MenuOptions.defaultOptions;

    public Menu(String title, Menu parent, User user) {
        this.title = title;
        this.items = new ArrayList<>();
        this.parent = parent;
        this.user = user;
    }

    public Menu(String title, Menu parent, User user, MenuOptions options) {
        this.title = title;
        this.items = new ArrayList<>();
        this.parent = parent;
        this.user = user;
        this.options = options;
    }

    public Menu(String title, Menu parent, User user, ArrayList<IRunnableMenu> items) {
        this.title = title;
        this.items = items;
        this.parent = parent;
        this.user = user;
    }

    public void addItem(IRunnableMenu menu) {
        items.add(menu);
    }

    /**
     * Displays children menus or information.
     * Using in {@link #run(Menu)}
     */
    public void display() {
        if (options != null && options.isUseBackButton()) {
            printBackButton();
        }
        for (int i = 0; i < items.size(); i++) {
            assert options != null;
            if (options.getDelimiterAfter() != -1 && options.getDelimiterAfter() == i + 1) {
                printDelimiter("=", 60);
            }
            if (items.get(i) instanceof MenuItem item) {
                System.out.println((i + 1) + ". " + item.getTitle());
            }
            if (items.get(i) instanceof Menu item) {
                System.out.println((i + 1) + ". " + item.getTitle());
            }
        }
        if (options != null && options.isUseExitButton()) {
            printExitButton();
        }
    }

    public String getTitle() {
        return title;
    }

    protected void printTitle() {
        System.out.println(title);
        printDelimiter(title.length());
    }

    protected void printDelimiter(String d, int length) {
        System.out.println(d.repeat(length));
    }


    protected void printDelimiter(int length) {
        System.out.println("=".repeat(length));
    }

    @Override
    public void run(Menu parent) {
        Scanner input = new Scanner(System.in);
        display();
        System.out.print("Enter your choice (number): ");
        int choice = input.nextInt();
        checkChoice(choice);
    }

    public MenuOptions getOptions() {
        return options;
    }

    public List<IRunnableMenu> getItems() {
        return items;
    }

    public Menu getParent() {
        return parent;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    protected void printBackButton() {
        System.out.println("0. Back");
    }

    protected void printExitButton() {
        System.out.println("0. Exit");
    }

    protected void checkChoice(int choice) {
        if (choice == 0) {
            if (options != null && options.isUseBackButton()) {
                back();
            } else if (options != null && options.isUseExitButton()) {
                System.exit(0);
            }
            else {
                System.out.println("Invalid choice. Please try again.");
                run(parent);
            }
        } else if (choice > 0 && choice <= items.size()) {
            items.get(choice - 1).run(this);
        } else {
            System.out.println("Invalid choice. Please try again.");
            run(parent);
        }
    }

    /**
     * Backs to the parent menu
     */
    public void back() {
        if (parent != null) {
            parent.run(parent.parent);
        }
    }
}
