package kz.aitu.oop.practice.marketplace.pkg.menus;

public interface IRunnableMenu {
    void run(Menu parent);
}
