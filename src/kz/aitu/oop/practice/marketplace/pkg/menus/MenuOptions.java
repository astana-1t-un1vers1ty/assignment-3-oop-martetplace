package kz.aitu.oop.practice.marketplace.pkg.menus;

public class MenuOptions {
    public static final MenuOptions defaultOptions = new MenuOptions().UseBackButton(false).setDelimiterAfter(-1);
    private boolean useBackButton;
    private boolean useExitButton;
    private int delimiterAfter;

    public MenuOptions UseBackButton() {
        useBackButton = true;
        useExitButton = false;
        return this;
    }

    public MenuOptions UseBackButton(boolean backButton) {
        useBackButton = backButton;
        useExitButton = false;
        return this;
    }

    public MenuOptions UseExitButton() {
        useBackButton = false;
        useExitButton = true;
        return this;
    }

    public MenuOptions UseExitButton(boolean exitButton) {
        useBackButton = exitButton;
        return this;
    }

    public boolean isUseBackButton() {
        return useBackButton;
    }

    public boolean isUseExitButton() {
        return useExitButton;
    }

    public int getDelimiterAfter() {
        return delimiterAfter;
    }

    public MenuOptions setDelimiterAfter(int delimiterAfter) {
        this.delimiterAfter = delimiterAfter;
        return this;
    }
}
