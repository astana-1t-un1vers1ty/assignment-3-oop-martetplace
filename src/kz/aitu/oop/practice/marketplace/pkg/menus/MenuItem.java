package kz.aitu.oop.practice.marketplace.pkg.menus;

import kz.aitu.oop.practice.marketplace.models.User;

public class MenuItem implements IRunnableMenu {
    private final String title;
    private final IRunnableMenu action;
    private User user;
    public MenuItem(String title, User user, IRunnableMenu action) {
        this.title = title;
        this.user = user;
        this.action = action;
    }

    @Override
    public void run(Menu parent) {
        action.run(parent);
    }

    public String getTitle() {
        return title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
