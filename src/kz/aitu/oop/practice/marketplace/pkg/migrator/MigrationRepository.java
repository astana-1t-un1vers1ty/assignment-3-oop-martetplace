package kz.aitu.oop.practice.marketplace.pkg.migrator;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;


public class MigrationRepository {
    Connection connection;
    public MigrationRepository(Connection connection) {
        this.connection = connection;
    }

    public Optional<Migration> getById(int id) {
        String sql = "SELECT * FROM migrations.history WHERE id=" + id;
        Optional<Migration> migration = Optional.empty();
        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                migration = Optional.of(new Migration(
                        resultSet.getInt("id"),
                        resultSet.getString("filename"),
                        resultSet.getString("hash"),
                        resultSet.getBoolean("executed")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return migration;
    }

    public Optional<Migration> getByName(String filename) {
        String sql = "SELECT * FROM migrations.history WHERE filename=?";
        Optional<Migration> migration = Optional.empty();
        try (var statement = connection.prepareStatement(sql)) {
            statement.setString(1, filename);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                migration = Optional.of(new Migration(
                    resultSet.getInt("id"),
                    resultSet.getString("filename"),
                    resultSet.getString("hash"),
                    resultSet.getBoolean("executed")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return migration;
    }

    public int save(Migration migration) {
        String message = "The migration to be added should not be null";
        Migration nonNullMigration = Objects.requireNonNull(migration, message);
        String sql = "INSERT INTO migrations.history(filename, hash, executed) VALUES(?, ?, ?)";
        int generatedId = 0;
        try (var statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, nonNullMigration.filename);
            statement.setString(2, nonNullMigration.hash);
            statement.setBoolean(3, nonNullMigration.isExecuted());

            int numberOfInsertedRows = statement.executeUpdate();
            if (numberOfInsertedRows > 0) {
                try (ResultSet resultSet = statement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        generatedId = resultSet.getInt(1);
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return generatedId;
    }

    public Migration[] getAll(Connection connection) {
        String sql = "SELECT * FROM migrations.history";
        var migrations = new ArrayList<Migration>();
        try (var statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) {
                var migration = new Migration(
                        resultSet.getInt("id"),
                        resultSet.getString("filename"),
                        resultSet.getString("hash"),
                        resultSet.getBoolean("executed")
                );
                migrations.add(migration);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return migrations.toArray(new Migration[0]);
    }

    public void update(Migration migration) {
        String sql = "UPDATE migrations.history SET filename = ?, hash = ?, executed = ? WHERE id=?";
        try (var statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, migration.filename);
            statement.setString(2, migration.hash);
            statement.setBoolean(3, migration.isExecuted());
            statement.setInt(4, migration.id);
            statement.executeUpdate();
        } catch (SQLException ignored) { }
    }
}
