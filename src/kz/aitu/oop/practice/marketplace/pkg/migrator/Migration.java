package kz.aitu.oop.practice.marketplace.pkg.migrator;

public class Migration {
    public final int id;
    public final String filename;
    public final String hash;
    private boolean executed;

    public Migration(int id, String filename, String hash, boolean executed) {
        this.id = id;
        this.filename = filename;
        this.hash = hash;
        this.executed = executed;
    }


    public boolean isExecuted() {
        return executed;
    }

    public void setExecuted(boolean executed) {
        this.executed = executed;
    }
}
