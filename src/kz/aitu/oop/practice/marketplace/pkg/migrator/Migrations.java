package kz.aitu.oop.practice.marketplace.pkg.migrator;

import kz.aitu.oop.practice.marketplace.helpers.Hasher;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class Migrations {
    public Migrations(Connection connection) {
        this.connection = connection;
    }

    private final Connection connection;
    private final List<File> migrations = new ArrayList<>();

    public void migrate() {
        try(var statement = connection.createStatement()) {
            var basePath = new File("").getAbsolutePath();
            var rootMigrationsDir = new File(basePath + "\\src\\kz\\aitu\\oop\\practice\\marketplace\\db\\migrations");
            loadMigrations(rootMigrationsDir.listFiles() != null ? Objects.requireNonNull(rootMigrationsDir.listFiles()) :  new File[] {}, 0, 0);
            executeFirstMigration(connection);
            var repo = new MigrationRepository(connection);
            Scanner scanner;
            for (var migration : migrations) {
                var migrationRepository = new MigrationRepository(connection);
                var dbmigtation = repo.getByName(migration.getName());
                var digest = MessageDigest.getInstance("SHA-256");
                scanner = new Scanner(migration);
                StringBuilder data = new StringBuilder();
                while (scanner.hasNextLine()) {
                    data.append(scanner.nextLine());
                }
                var hash = digest.digest(data.toString().getBytes(StandardCharsets.UTF_8));
                if (dbmigtation.isEmpty()) {
                    var id = migrationRepository.save(new Migration(-1, migration.getName(), Hasher.bytesToHex(hash), false));
                    dbmigtation = migrationRepository.getById(id);
                }

                if (dbmigtation.isPresent()) {
                    if (!dbmigtation.get().hash.equals("")) {
                        if (!Hasher.bytesToHex(hash).equals(dbmigtation.get().hash)) {
                            throw new RuntimeException("The hash of '" + migration.getName() + "' does not equals");
                        }
                    }

                    if (!dbmigtation.get().isExecuted()) {
                        statement.executeUpdate(data.toString());
                        dbmigtation.get().setExecuted(true);
                        migrationRepository.update(dbmigtation.get());
                    }
                }
            }
        } catch (SQLException | FileNotFoundException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private void loadMigrations(File[] a, int i, int lvl) {
        if(i == a.length) {
            return;
        }
        if(a[i].isFile()) {
            var name = a[i].getName().split("\\.");
            if (name[name.length - 1].equals("sql"))
                migrations.add(a[i]);
        }
        else if(a[i].isDirectory()) {
            loadMigrations(Objects.requireNonNull(a[i].listFiles()), 0, lvl + 1);
        }
        loadMigrations(a, i + 1, lvl);
    }

    private void executeFirstMigration(Connection connection) {
        var sql = """
                CREATE SCHEMA IF NOT EXISTS migrations;

                CREATE TABLE IF NOT EXISTS migrations.history(
                    id SERIAL NOT NULL PRIMARY KEY,
                    filename VARCHAR(256),
                    hash VARCHAR(256),
                    executed BOOLEAN DEFAULT FALSE
                );""";
        try(var statement = connection.createStatement()) {
            statement.executeQuery(sql);
        } catch (Exception ignored) { }
    }
}
