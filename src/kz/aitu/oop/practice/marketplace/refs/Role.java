package kz.aitu.oop.practice.marketplace.refs;

public enum Role {
    Guest,
    User,
    Vendor
}
