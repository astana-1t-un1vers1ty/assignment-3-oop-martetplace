package kz.aitu.oop.practice.marketplace.helpers;


import kz.aitu.oop.practice.marketplace.db.JDBCConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

public class CharacteristicHelper {
    private static final Connection connection = JDBCConnection.instance.connection;

    public static String[] getCharacteristicKeys(int categoryId) {
        var characteristicKeys = new ArrayList<String>();
        try {
            String sql = "select jsonb_object_keys(\"characteristics\") as c from marketplace.products " +
                    "where category_id=? " +
                    "group by c;";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, categoryId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                characteristicKeys.add(rs.getString("c"));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return characteristicKeys.toArray(new String[0]);
    }

    public static Object[] getCharacteristicValues(int categoryId, String key) {
        var characteristicValues = new HashSet<>();
        try {
            String sql = "select \"characteristics\"->>? as c from marketplace.products where category_id=? group by \"characteristics\", marketplace.products.\"characteristics\"->>?;";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, key);
            stmt.setInt(2, categoryId);
            stmt.setString(3, key);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                characteristicValues.add(rs.getObject("c"));
            }
        } catch (SQLException e){
            throw new RuntimeException(e);
        }
        return characteristicValues.toArray(new Object[0]);
    }
}
