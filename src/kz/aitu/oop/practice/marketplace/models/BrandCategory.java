package kz.aitu.oop.practice.marketplace.models;

public class BrandCategory {
    private Brand brand;
    private Category category;

    public BrandCategory(Brand brand, Category category) {
        this.brand = brand;
        this.category = category;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
