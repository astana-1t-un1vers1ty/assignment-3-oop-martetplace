package kz.aitu.oop.practice.marketplace.models;

import kz.aitu.oop.practice.marketplace.models.base.Table;

public class Vendor extends Table {
    private String name;
    private Address[] addresses;

    public Vendor(int id, String name, Address[] addresses) {
        super(id);
        this.name = name;
        this.addresses = addresses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address[] getAddresses() {
        return addresses;
    }

    public void setAddresses(Address[] addresses) {
        this.addresses = addresses;
    }

    
}
