package kz.aitu.oop.practice.marketplace.models;

public class VendorProduct {
    private Vendor vendor;
    private Product product;
    private double price;

    public VendorProduct(Vendor vendor, Product product, double price) {
        this.vendor = vendor;
        this.product = product;
        this.price = price;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
