package kz.aitu.oop.practice.marketplace.models;

import kz.aitu.oop.practice.marketplace.models.base.Table;
import kz.aitu.oop.practice.marketplace.refs.Role;


public class User extends Table {
    private String fullName;
    private final Role role;
    private String login;
    private String password;
    private final int vendorId;

    public User(int id, String fullName, Role role, String login, String password, int vendorId) {
        super(id);
        this.fullName = fullName;
        this.role = role;
        this.login = login;
        this.password = password;
        this.vendorId = vendorId;
    }

    public static User Guest() {
        return new User(0, "Unknown", Role.Guest, "guest", "guest", 0);
    }

    public int getVendorId() {
        return vendorId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLogin() {
        return login;
    }

    public Role getRole() {
        return role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLogin(String login){
        this.login = login;
    }
}
