package kz.aitu.oop.practice.marketplace.models;

import kz.aitu.oop.practice.marketplace.models.base.Table;
import kz.aitu.oop.practice.marketplace.refs.Role;

public class VendorUser extends User{
    private Vendor vendor;

    public VendorUser(int id, String fullName, Role role, String login, String password, int vendorId, Vendor vendor) {
        super(id, fullName, role, login, password, vendorId);
        this.vendor=vendor;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }
}
