package kz.aitu.oop.practice.marketplace.models;

import kz.aitu.oop.practice.marketplace.models.base.Table;

public class Category extends Table {
    private String nameEn;
    private int parentId;

    public Category(int id, String nameEn, int parentId) {
        super(id);
        this.nameEn = nameEn;
        this.parentId = parentId;
    }

    public String getName() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }
}
