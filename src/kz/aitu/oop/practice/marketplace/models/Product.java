package kz.aitu.oop.practice.marketplace.models;

import kz.aitu.oop.practice.marketplace.models.base.ICharacteristic;
import kz.aitu.oop.practice.marketplace.models.base.Table;

import java.util.Map;

public class Product extends Table{
    private String name;
    private final Brand brand;
    private final Category category;
    private final Map<String, Object> characteristics;
    public Product(int id, String name, Brand brand, Category category, Map<String, Object> characteristics) {
        super(id);
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.characteristics = characteristics;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brand getBrand() {
        return brand;
    }

    public Category getCategory() {
        return category;
    }

    public Map<String, Object> getCharacteristics() { return characteristics; }

}
