package kz.aitu.oop.practice.marketplace.models;

import kz.aitu.oop.practice.marketplace.models.base.Table;

public class Country extends Table {
    private String name;

    public Country(int id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
