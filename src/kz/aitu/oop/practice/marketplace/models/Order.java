package kz.aitu.oop.practice.marketplace.models;

import kz.aitu.oop.practice.marketplace.models.base.Table;

public class Order extends Table {
    private Product product;
    private Vendor vendor;

    private String address;

    private boolean completed;
    public Order(int id, Product product, Vendor vendor, String address, boolean completed) {
        super(id);
        this.product = product;
        this.vendor = vendor;
        this.address = address;
        this.completed = completed;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
