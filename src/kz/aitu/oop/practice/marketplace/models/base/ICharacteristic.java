package kz.aitu.oop.practice.marketplace.models.base;

import java.util.Dictionary;

public interface ICharacteristic {
    Dictionary<String, Object> getCharacteristic(String key, Object value);
    Dictionary<String, Object>[] getCharacteristics();
}
