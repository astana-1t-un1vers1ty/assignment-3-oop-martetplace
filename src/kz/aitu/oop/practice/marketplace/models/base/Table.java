package kz.aitu.oop.practice.marketplace.models.base;

public abstract class Table {
    public final int id;

    protected Table(int id) {
        this.id = id;
    }

    public int getId(){
        return this.id;
    }
}
