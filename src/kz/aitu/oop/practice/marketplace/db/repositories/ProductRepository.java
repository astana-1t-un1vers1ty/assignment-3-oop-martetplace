package kz.aitu.oop.practice.marketplace.db.repositories;

import com.google.gson.Gson;
import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.base.Repository;
import kz.aitu.oop.practice.marketplace.models.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class ProductRepository implements Repository<Integer, Product> {
    public static final ProductRepository instance = new ProductRepository();
    private final Connection connection;
    private ProductRepository(){
        this.connection = JDBCConnection.instance.connection;
    }

    @Override
    public Product get(int id) {
        try {
            String sql = "SELECT * FROM marketplace.products WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return new Product(
                    rs.getInt("id"),
                    rs.getString("name"),
                    BrandRepository.instance.get(rs.getInt("brand_id")),
                    CategoryRepository.instance.get(rs.getInt("category_id")),
                    (Map<String, Object>)new Gson().fromJson(rs.getString("characteristics"), Map.class)
                );
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<Product> getAll() {
        return null;
    }

    @Override
    public Integer save(Product t) {
        Integer generatedId = null;
        try {
            String sql = "INSERT INTO marketplace.products(id, \"name\", brand_id, category_id, characteristics) VALUES(?, ?, ?, ?, ?::JSONB);";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, getLastId() + 1);
            stmt.setString(2, t.getName());
            stmt.setInt(3, t.getBrand().id);
            stmt.setInt(4, t.getCategory().id);
            stmt.setObject(5, new Gson().toJson(t.getCharacteristics()));
            int numberOfInsertedRows = stmt.executeUpdate();
            if (numberOfInsertedRows > 0) {
                try (ResultSet resultSet = stmt.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        generatedId = resultSet.getInt(1);
                    }
                }
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return generatedId;
    }

    @Override
    public void update(Product t) {

    }

    @Override
    public void delete(Integer t) {

    }

    public Product[] getProductsLike(String string) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            String sql = "SELECT id, \"name\", brand_id, category_id, \"characteristics\" FROM marketplace.products WHERE " + prepareSearchString(string) + ";";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                products.add(
                    new Product(
                        rs.getInt("id"),
                        rs.getString("name"),
                        BrandRepository.instance.get(rs.getInt("brand_id")),
                        CategoryRepository.instance.get(rs.getInt("category_id")),
                        (Map<String, Object>)new Gson().fromJson(rs.getString("characteristics"), Map.class)
                    )
                );
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return products.toArray(new Product[0]);
    }

    public void getCharacteristicsById() {

    }

    public Product[] getByCategory(int id) {
        var products = new ArrayList<Product>();
        try {
            String sql = "SELECT * FROM marketplace.products WHERE category_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                products.add(new Product(
                    rs.getInt("id"),
                    rs.getString("name"),
                    BrandRepository.instance.get(rs.getInt("brand_id")),
                    CategoryRepository.instance.get(rs.getInt("category_id")),
                    (Map<String, Object>)new Gson().fromJson(rs.getString("characteristics"), Map.class)
                ));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return products.toArray(new Product[0]);
    }

    public Product getByName(String name) {
        try {
            String sql = "SELECT * FROM marketplace.products WHERE \"name\"=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                return new Product(
                    rs.getInt("id"),
                    rs.getString("name"),
                    BrandRepository.instance.get(rs.getInt("brand_id")),
                    CategoryRepository.instance.get(rs.getInt("category_id")),
                    (Map<String, Object>)new Gson().fromJson(rs.getString("characteristics"), Map.class)
                );
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    private int getLastId() {
        try {
            String sql = "select MAX(id) from marketplace.products;";
            var stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()){
                return rs.getInt(1);
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return 0;
    }

    private String prepareSearchString(String search) {
        var searchTags = search.split(" ");
        for (int i = 0; i < searchTags.length; i++) {
            searchTags[i] = "\"name\" ILIKE '%' || '" + searchTags[i] + "' || '%'";
        }
        return String.join(" AND ", searchTags);
    }
}
