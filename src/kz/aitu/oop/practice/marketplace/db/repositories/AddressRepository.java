package kz.aitu.oop.practice.marketplace.db.repositories;

import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.base.Repository;
import kz.aitu.oop.practice.marketplace.models.Address;
import kz.aitu.oop.practice.marketplace.models.City;
import kz.aitu.oop.practice.marketplace.models.Country;
import kz.aitu.oop.practice.marketplace.models.Region;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class AddressRepository implements Repository<Integer, Address> {
    public static final AddressRepository instance = new AddressRepository();
    private final Connection connection;
    private AddressRepository(){
        this.connection = JDBCConnection.instance.connection;
    }

    @Override
    public Address get(int id) {
        try{
            String sql = "SELECT * FROM marketplace.addresses WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int addressId = rs.getInt("id");
                int countryId = rs.getInt("country_id");
                int regionId = rs.getInt("region_id");
                int cityId = rs.getInt("city_id");
                int vendorId = rs.getInt("vendor_id");
                Country country = new CountryRepository().get(countryId);
                Region region= new RegionRepository().get(regionId);
                City city = new CityRepository().get(cityId);
                String address = rs.getString("address");
                return new Address(addressId, country, region, city, address, vendorId);
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<Address> getAll() {
        ArrayList<Address> addresses= new ArrayList<>();
        try{
            String sql = "SELECT * FROM marketplace.addresses";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int addressId = rs.getInt("id");
                int countryId = rs.getInt("country_id");
                int regionId = rs.getInt("region_id");
                int cityId = rs.getInt("city_id");
                int vendorId = rs.getInt("vendor_id");
                Country country = new CountryRepository().get(countryId);
                Region region= new RegionRepository().get(regionId);
                City city = new CityRepository().get(cityId);
                String address = rs.getString("address");
                addresses.add(new Address(addressId, country, region, city, address, vendorId));
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return addresses;
    }

    @Override
    public Integer save(Address address) {
        try{
            String sql = "INSERT INTO marketplace.addresses VALUES(?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, address.getId());
            stmt.setInt(2, address.getCountry().getId());
            stmt.setInt(3, address.getRegion().getId());
            stmt.setInt(4, address.getCity().getId());
            stmt.setString(5, address.getAddress());
            stmt.setInt(6, address.getVendorId());
            int numberOfInsertedRows = stmt.executeUpdate();
            if(numberOfInsertedRows>0){
                try(ResultSet rs = stmt.getGeneratedKeys()){
                    if(rs.next()){
                        return rs.getInt(1);
                    }
                }
            }

        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Address address) {
        try{
            String sql = "UPDATE marketplace.addresses SET country_id=?," +
                    "region_id=?,"+
                    "city_id=?,"+
                    "address=?,"+
                    "vendor_id=?)"+
                    "WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, address.getCountry().getId());
            stmt.setInt(2, address.getRegion().getId());
            stmt.setInt(3, address.getCity().getId());
            stmt.setString(4, address.getAddress());
            stmt.setInt(5, address.getVendorId());
            stmt.setInt(6, address.getId());
            stmt.executeUpdate();
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try{
            String sql = "DELETE FROM marketplace.addresses WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    public Collection<Address> getAllByVendorId(int id){
        ArrayList<Address> addresses= new ArrayList<>();
        try{
            String sql = "SELECT * FROM marketplace.addresses WHERE vendor_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int addressId = rs.getInt("id");
                int countryId = rs.getInt("country_id");
                int regionId = rs.getInt("region_id");
                int cityId = rs.getInt("city_id");
                int vendorId = rs.getInt("vendor_id");
                Country country = new CountryRepository().get(countryId);
                Region region= new RegionRepository().get(regionId);
                City city = new CityRepository().get(cityId);
                String address = rs.getString("address");
                addresses.add(new Address(addressId, country, region, city, address, vendorId));
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return addresses;
    }
}
