package kz.aitu.oop.practice.marketplace.db.repositories;

import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.base.Repository;
import kz.aitu.oop.practice.marketplace.models.Address;
import kz.aitu.oop.practice.marketplace.models.Vendor;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;


public class VendorRepository implements Repository<Integer, Vendor> {
    public static final VendorRepository instance = new VendorRepository();
    private final Connection connection;
    private VendorRepository(){
        this.connection = JDBCConnection.instance.connection;
    }

    @Override
    public Vendor get(int id) {
        try {
            String sql = "SELECT * FROM marketplace.vendors WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int vendorId = rs.getInt("id");
                String vendorName = rs.getString("name");
                Collection<Address> addresses = AddressRepository.instance.getAllByVendorId(vendorId);
                return new Vendor(vendorId, vendorName, addresses.toArray(new Address[0]));
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    public Vendor[] getByProductId(int product_id) {
        Collection<Vendor> vendors = new ArrayList<>();
        try {
            String sql = "SELECT * FROM marketplace.vendors";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                int vendorId = rs.getInt("id");
                String vendorName = rs.getString("name");
                Collection<Address> addresses = AddressRepository.instance.getAllByVendorId(vendorId);
                vendors.add(new Vendor(vendorId, vendorName, addresses.toArray(new Address[0])));
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return vendors.toArray(new Vendor[0]);
    }

    @Override
    public Collection<Vendor> getAll() {
        Collection<Vendor> vendors = new ArrayList<>();
        try {
            String sql = "SELECT * FROM marketplace.vendors";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                int vendorId = rs.getInt("id");
                String vendorName = rs.getString("name");
                Collection<Address> addresses = AddressRepository.instance.getAllByVendorId(vendorId);
                vendors.add(new Vendor(vendorId, vendorName, addresses.toArray(new Address[0])));
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return vendors;
    }

    @Override
    public Integer save(Vendor vendor) {
        try{
            String sql = "INSERT INTO marketplace.vendors(name) VALUES(?)";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, vendor.getName());
            int numberOfInsertedRows = stmt.executeUpdate();
            if(numberOfInsertedRows>0){
                try(ResultSet rs = stmt.getGeneratedKeys()){
                    if(rs.next()){
                        return rs.getInt(1);
                    }
                }
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Vendor vendor) {
        try{
            String sql = "UPDATE marketplace.vendors SET name=? WHERE id=?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1,vendor.getName());
            stmt.setInt(2, vendor.getId());
            stmt.executeUpdate();
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            String sql = "DELETE FROM marketplace.vendors WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }
}
