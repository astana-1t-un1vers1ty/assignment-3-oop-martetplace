package kz.aitu.oop.practice.marketplace.db.repositories.base;

import kz.aitu.oop.practice.marketplace.models.Address;

import java.util.Collection;


public interface Repository<TId, TModel> {
    TModel get(int id);
    Collection<TModel> getAll();
    TId save(TModel t);
    void update(TModel t);
    void delete(TId t);
}

