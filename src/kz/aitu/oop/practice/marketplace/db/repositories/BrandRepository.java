package kz.aitu.oop.practice.marketplace.db.repositories;

import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.base.Repository;
import kz.aitu.oop.practice.marketplace.models.Brand;

import java.sql.*;
import java.util.*;

public class BrandRepository implements Repository<Integer, Brand> {
    public static final BrandRepository instance = new BrandRepository();
    private final Connection connection;
    private BrandRepository(){
        this.connection = JDBCConnection.instance.connection;
    }

    @Override
    public Brand get(int id) {
        try {
            String sql = "SELECT * FROM marketplace.brands WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int brandId = rs.getInt("id");
                String brandName = rs.getString("name");
                return new Brand(brandId, brandName);
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<Brand> getAll() {
        ArrayList<Brand> brands = new ArrayList<>();
        try {
            String sql = "SELECT * FROM marketplace.brands";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int brandId = rs.getInt("id");
                String brandName = rs.getString("name");
                brands.add(new Brand(brandId, brandName));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return brands;
    }

    @Override
    public Integer save(Brand brand) {
        try{
            String sql = "INSERT INTO marketplace.brands(name) VALUES(?)";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, brand.getName());
            int numberOfInsertedRows = stmt.executeUpdate();
            if(numberOfInsertedRows>0){
                try(ResultSet rs = stmt.getGeneratedKeys()){
                    if(rs.next()){
                        return rs.getInt(1);
                    }
                }
            }

        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Brand brand) {
        try{
            String sql = "UPDATE marketplace.brands SET name=? WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, brand.getName());
            stmt.setInt(2, brand.getId());
            stmt.executeUpdate();
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try{
            String sql = "DELETE FROM marketplace.brands WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    public Brand getByName(String name) {
        try {
            String sql = "SELECT * FROM marketplace.brands WHERE LOWER(name)=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, name.toLowerCase(Locale.ROOT));
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return new Brand(rs.getInt("id"), rs.getString("name"));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }
}
