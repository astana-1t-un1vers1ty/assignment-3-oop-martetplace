package kz.aitu.oop.practice.marketplace.db.repositories;

import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.models.VendorProduct;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;



public class VendorProductRepository {
    public static final VendorProductRepository instance = new VendorProductRepository();
    private final Connection connection;
    private VendorProductRepository(){
        this.connection = JDBCConnection.instance.connection;
    }

    public VendorProduct[] getByProductId(int id) {
        ArrayList<VendorProduct> vendorProducts = new ArrayList<>();
        try {
            String sql = "SELECT * FROM marketplace.vendors_products WHERE product_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                int vendorId = rs.getInt("vendor_id");
                int productId = rs.getInt("product_id");
                double price = rs.getDouble("price");
                vendorProducts.add(
                    new VendorProduct(
                        VendorRepository.instance.get(vendorId),
                        ProductRepository.instance.get(productId),
                        price
                    )
                );
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return vendorProducts.toArray(new VendorProduct[0]);
    }

    public VendorProduct[] getByVendorId(int id) {
        ArrayList<VendorProduct> vendorProducts = new ArrayList<>();
        try {
            String sql = "SELECT * FROM marketplace.vendors_products WHERE vendor_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                int vendorId = rs.getInt("vendor_id");
                int productId = rs.getInt("product_id");
                double price = rs.getDouble("price");
                vendorProducts.add(
                        new VendorProduct(
                                VendorRepository.instance.get(vendorId),
                                ProductRepository.instance.get(productId),
                                price
                        )
                );
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return vendorProducts.toArray(new VendorProduct[0]);
    }

    public Double getPriceByVendorAndProductId(int venodrId, int productId){
        try {
            String sql = "SELECT * FROM marketplace.vendors_products WHERE vendor_id=? AND product_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, venodrId);
            stmt.setInt(2, productId);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return rs.getDouble("price");
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    public Collection<VendorProduct> getAll() {
        ArrayList<VendorProduct> vendorProducts = new ArrayList<>();
        try {
            String sql = "SELECT * FROM marketplace.vendors_products";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int vendorId = rs.getInt("vendor_id");
                int productId = rs.getInt("product_id");
                double price = rs.getDouble("price");
                vendorProducts.add(new VendorProduct(VendorRepository.instance.get(vendorId),
                        ProductRepository.instance.get(productId), price));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return vendorProducts;
    }

    public Integer save(VendorProduct vendorProduct) {
        try{
            String sql = "INSERT INTO marketplace.vendors_products(vendor_id, product_id, price) VALUES(?,?,?)";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, vendorProduct.getVendor().getId());
            stmt.setInt(2, vendorProduct.getProduct().getId());
            stmt.setDouble(3, vendorProduct.getPrice());
            int numberOfInsertedRows = stmt.executeUpdate();
            if(numberOfInsertedRows>0){
                try(ResultSet rs = stmt.getGeneratedKeys()){
                    if(rs.next()){
                        return rs.getInt(1);
                    }
                }
            }

        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    public void update(VendorProduct vendorProduct) {
        try{
            String sql = "UPDATE marketplace.vendors_products SET price=?  WHERE product_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setDouble(1, vendorProduct.getPrice());
            stmt.setInt(2, vendorProduct.getProduct().getId());
            stmt.executeUpdate();
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    public void delete(int vendorId, int productId) {
        try {
            String sql = "DELETE FROM marketplace.vendors_products WHERE vendor_id=? AND product_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, vendorId);
            stmt.setInt(2, productId);
            stmt.executeUpdate();
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }
}
