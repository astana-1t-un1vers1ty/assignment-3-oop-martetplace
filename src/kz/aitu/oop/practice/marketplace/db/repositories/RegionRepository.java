package kz.aitu.oop.practice.marketplace.db.repositories;

import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.base.Repository;
import kz.aitu.oop.practice.marketplace.models.Region;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class RegionRepository implements Repository<Integer, Region> {
    public static final RegionRepository instance = new RegionRepository();
    private final Connection connection;
    public RegionRepository(){
        this.connection = JDBCConnection.instance.connection;
    }

    @Override
    public Region get(int id) {
        try {
            String sql = "SELECT * FROM refs.regions WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int regionId = rs.getInt("id");
                String regionName = rs.getString("name");
                int countryId = rs.getInt("country_id");
                return new Region(regionId, regionName, countryId);
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<Region> getAll() {
        ArrayList<Region> regions = new ArrayList<>();
        try {
            String sql = "SELECT * FROM refs.regions";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int regionId = rs.getInt("id");
                String regionName = rs.getString("name");
                int countryId = rs.getInt("country_id");
                regions.add(new Region(regionId, regionName, countryId));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return regions;
    }

    @Override
    public Integer save(Region region) {
        try{
            String sql = "INSERT INTO refs.regions(name, country_id) VALUES(?, ?)";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, region.getName());
            stmt.setInt(2,region.getCountryId());
            int numberOfInsertedRows = stmt.executeUpdate();
            if(numberOfInsertedRows>0){
                try(ResultSet rs = stmt.getGeneratedKeys()){
                    if(rs.next()){
                        return rs.getInt(1);
                    }
                }
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Region region) {
        try{
            String sql = "UPDATE refs.regions SET name=?, country_id=? WHERE id=?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1,region.getName());
            stmt.setInt(2, region.getCountryId());
            stmt.setInt(3, region.getId());
            stmt.executeUpdate();
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            String sql = "DELETE FROM refs.regions WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    public Region[] getByCountryId(int id) {
        ArrayList<Region> regions = new ArrayList<>();
        try {
            String sql = "SELECT * FROM refs.regions WHERE country_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int regionId = rs.getInt("id");
                String regionName = rs.getString("name");
                int countryId = rs.getInt("country_id");
                regions.add(new Region(regionId, regionName, countryId));
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return regions.toArray(new Region[0]);
    }
}
