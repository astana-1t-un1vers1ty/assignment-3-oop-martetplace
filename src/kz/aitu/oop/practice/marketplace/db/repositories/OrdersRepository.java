package kz.aitu.oop.practice.marketplace.db.repositories;

import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.base.Repository;
import kz.aitu.oop.practice.marketplace.models.Order;
import kz.aitu.oop.practice.marketplace.models.Product;
import kz.aitu.oop.practice.marketplace.models.Vendor;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class OrdersRepository implements Repository<Integer, Order> {
    public static final OrdersRepository instance = new OrdersRepository();
    private final Connection connection;
    public OrdersRepository(){
        this.connection = JDBCConnection.instance.connection;
    }

    @Override
    public Order get(int id) {
        try {
            String sql = "SELECT * FROM marketplace.orders WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int orderId = rs.getInt("id");
                Product product = ProductRepository.instance.get(rs.getInt("product_id"));
                Vendor vendor = VendorRepository.instance.get(rs.getInt("vendor_id"));
                String address = rs.getString("address");
                boolean completed = rs.getBoolean("completed");
                return new Order(orderId, product, vendor, address, completed);
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<Order> getAll() {
        var orders = new ArrayList<Order>();
        try {
            String sql = "SELECT * FROM marketplace.orders";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int orderId = rs.getInt("id");
                Product product = ProductRepository.instance.get(rs.getInt("product_id"));
                Vendor vendor = VendorRepository.instance.get(rs.getInt("vendor_id"));
                String address = rs.getString("address");
                boolean completed = rs.getBoolean("completed");
                orders.add(new Order(orderId, product, vendor, address, completed));
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return orders;
    }

    public Collection<Order> getAllByVendorId(int id) {
        var orders = new ArrayList<Order>();
        try {
            String sql = "SELECT * FROM marketplace.orders WHERE vendor_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int orderId = rs.getInt("id");
                Product product = ProductRepository.instance.get(rs.getInt("product_id"));
                Vendor vendor = VendorRepository.instance.get(rs.getInt("vendor_id"));
                String address = rs.getString("address");
                boolean completed = rs.getBoolean("completed");
                orders.add(new Order(orderId, product, vendor, address, completed));
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return orders;
    }


    @Override
    public Integer save(Order order) {
        try{
            String sql = "INSERT INTO marketplace.orders(product_id, vendor_id, address) VALUES(?, ?, ?)";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, order.getProduct().getId());
            stmt.setInt(2, order.getVendor().getId());
            stmt.setString(3, order.getAddress());
            int numberOfInsertedRows = stmt.executeUpdate();
            if(numberOfInsertedRows>0){
                try(ResultSet rs = stmt.getGeneratedKeys()){
                    if(rs.next()){
                        return rs.getInt(1);
                    }
                }
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return  null;
    }

    @Override
    public void update(Order order) {
        try{
            String sql = "UPDATE marketplace.orders SET product_id=?, vendor_id=?, address=?, comleted=? WHERE id=?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, order.getProduct().getId());
            stmt.setInt(2, order.getVendor().getId());
            stmt.setString(3, order.getAddress());
            stmt.setBoolean(4, order.isCompleted());
            stmt.setInt(5, order.getId());
            stmt.executeUpdate();
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            String sql = "DELETE FROM marketplace.orders WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }
}
