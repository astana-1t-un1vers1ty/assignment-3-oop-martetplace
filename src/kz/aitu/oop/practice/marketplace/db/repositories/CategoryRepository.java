package kz.aitu.oop.practice.marketplace.db.repositories;

import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.base.Repository;
import kz.aitu.oop.practice.marketplace.models.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class CategoryRepository implements Repository<Integer, Category> {
    public static final CategoryRepository instance = new CategoryRepository();
    private final Connection connection;
    private CategoryRepository(){
        this.connection = JDBCConnection.instance.connection;
    }

    @Override
    public Category get(int id) {
        try {
            String sql = "SELECT * FROM marketplace.categories WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int categoryId = rs.getInt("id");
                String categoryName = rs.getString("name");
                int parentId = rs.getInt("parent_id");
                return new Category(categoryId, categoryName, parentId);
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<Category> getAll() {
        ArrayList<Category> categories = new ArrayList<>();
        try {
            String sql = "SELECT * FROM marketplace.categories";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int categoryId = rs.getInt("id");
                String categoryName = rs.getString("name");
                int parentId = rs.getInt("parent_id");
                categories.add(new Category(categoryId, categoryName, parentId));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return categories;
    }

    @Override
    public Integer save(Category category) {
        try{
            String sql = "INSERT INTO marketplace.categories(name, parent_id) VALUES(?, ?)";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, category.getName());
            stmt.setInt(2,category.getParentId());
            int numberOfInsertedRows = stmt.executeUpdate();
            if(numberOfInsertedRows>0){
                try(ResultSet rs = stmt.getGeneratedKeys()){
                    if(rs.next()){
                        return rs.getInt(1);
                    }
                }
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Category category) {
        try{
            String sql = "UPDATE marketplace.categories SET name=?, parent_id=? WHERE id=?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1,category.getName());
            stmt.setInt(2, category.getParentId());
            stmt.setInt(3, category.getId());
            stmt.executeUpdate();
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try{
            String sql = "DELETE FROM marketplace.categories WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    public Category[] getRootCategories() {
        ArrayList<Category> categories = new ArrayList<>();
        try {
            String sql = "SELECT * FROM marketplace.categories WHERE parent_id IS NULL";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int categoryId = rs.getInt("id");
                String categoryName = rs.getString("name");
                int parentId = rs.getInt("parent_id");
                categories.add(new Category(categoryId, categoryName, parentId));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return categories.toArray(new Category[0]);
    }

    public Category[] getByParent(int id) {
        ArrayList<Category> categories = new ArrayList<>();
        try {
            String sql = "SELECT * FROM marketplace.categories WHERE parent_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int categoryId = rs.getInt("id");
                String categoryName = rs.getString("name");
                int parentId = rs.getInt("parent_id");
                categories.add(new Category(categoryId, categoryName, parentId));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return categories.toArray(new Category[0]);
    }
}
