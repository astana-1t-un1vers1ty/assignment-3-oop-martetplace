package kz.aitu.oop.practice.marketplace.db.repositories;

import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.base.Repository;
import kz.aitu.oop.practice.marketplace.helpers.Hasher;
import kz.aitu.oop.practice.marketplace.models.User;
import kz.aitu.oop.practice.marketplace.models.Vendor;
import kz.aitu.oop.practice.marketplace.models.VendorUser;
import kz.aitu.oop.practice.marketplace.refs.Role;

import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;


public class UserRepository implements Repository<Integer, User> {
    public static final UserRepository instance = new UserRepository();
    private final Connection connection;
    private UserRepository(){
        this.connection = JDBCConnection.instance.connection;
    }

    @Override
    public User get(int id) {
        try {
            String sql = "SELECT * FROM users.users WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int userId = rs.getInt("id");
                String fullName = rs.getString("fullName");
                Role role = Role.User;
                int vendorId = 0;
                Vendor vendor = getVendorByUserId(userId);
                String login = rs.getString("login");
                String password = rs.getString("password");
                if(vendor != null) {
                    role = Role.Vendor;
                    vendorId = vendor.getId();
                    return new VendorUser(userId, fullName, role, login, password, vendorId, vendor);
                }
                else {
                    return new User(userId, fullName, role, login, password, vendorId);
                }
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    Vendor getVendorByUserId(int userId){
        try{
            String sql = "SELECT * FROM marketplace.vendors_users WHERE user_id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int vendorId = rs.getInt("vendor_id");
                return VendorRepository.instance.get(vendorId);
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    public User getAuthorized() {
        try {
            String sql = "SELECT * FROM users.users WHERE is_authorized=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setBoolean(1,true);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int userId = rs.getInt("id");
                String fullName = rs.getString("fullName");
                Role role = Role.User;
                int vendorId = 0;
                Vendor vendor = getVendorByUserId(userId);
                String login = rs.getString("login");
                String password = rs.getString("password");
                if(vendor != null) {
                    role = Role.Vendor;
                    vendorId = vendor.getId();
                    return new VendorUser(userId, fullName, role, login, password, vendorId, vendor);
                }
                else {
                    return new User(userId, fullName, role, login, password, vendorId);
                }
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    public User getByLogin(String userLogin){
        try {
            String sql = "SELECT * FROM users.users WHERE login=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, userLogin);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int userId = rs.getInt("id");
                String fullName = rs.getString("fullName");
                Role role = Role.User;
                int vendorId = 0;
                Vendor vendor = getVendorByUserId(userId);
                String login = rs.getString("login");
                String password = rs.getString("password");
                if(vendor != null) {
                    role = Role.Vendor;
                    vendorId = vendor.getId();
                    return new VendorUser(userId, fullName, role, login, password, vendorId, vendor);
                }
                else {
                    return new User(userId, fullName, role, login, password, vendorId);
                }
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    public User getByLoginAndPassword(String userLogin, String userPassword){
        try {
            String sql = "SELECT * FROM users.users WHERE login=? AND password=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, userLogin);
            stmt.setString(2, Hasher.bytesToHex(userPassword.getBytes(StandardCharsets.UTF_8)));
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int userId = rs.getInt("id");
                String fullName = rs.getString("fullName");
                Role role = Role.User;
                int vendorId = 0;
                Vendor vendor = getVendorByUserId(userId);
                String login = rs.getString("login");
                String password = rs.getString("password");
                if(vendor != null) {
                    role = Role.Vendor;
                    vendorId = vendor.getId();
                    return new VendorUser(userId, fullName, role, login, password, vendorId, vendor);
                }
                else {
                    return new User(userId, fullName, role, login, password, vendorId);
                }
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    public void authorize(User user){
        try {
            String sql = "UPDATE users.users SET is_authorized=? WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setBoolean(1, true);
            stmt.setInt(2, user.getId());
            stmt.executeUpdate();
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    public void unauthorize(int id){
        try {
            String sql = "UPDATE users.users SET is_authorized=? WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setBoolean(1, false);
            stmt.setInt(2, id);
            stmt.executeUpdate();
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    @Override
    public Collection<User> getAll() {
        Collection<User> users= new ArrayList<>();
        try {
            String sql = "SELECT * FROM users.users";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                int userId = rs.getInt("id");
                String fullName = rs.getString("fullName");
                Role role = Role.User;
                int vendorId = 0;
                Vendor vendor = getVendorByUserId(userId);

                String login = rs.getString("login");
                String password = rs.getString("password");
                if(vendor != null) {
                    role = Role.Vendor;
                    vendorId = vendor.getId();
                    users.add(new VendorUser(userId, fullName, role, login, password, vendorId, vendor));
                }
                else {
                    users.add(new User(userId, fullName, role, login, password, vendorId));
                }

            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return users;
    }

    @Override
    public Integer save(User user) {
        try{
            String sql = "INSERT INTO users.users (fullName, login, password, is_authorized) VALUES(?,?,?,?)";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, user.getFullName());
            stmt.setString(2, user.getLogin());
            stmt.setString(3, user.getPassword());
            stmt.setBoolean(4, false);
            int numberOfInsertedRows = stmt.executeUpdate();
            if(numberOfInsertedRows>0){
                try(ResultSet rs = stmt.getGeneratedKeys()){
                    if(rs.next()){
                        return rs.getInt(1);
                    }
                }
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(User user) {
        try {
            String sql = "UPDATE users.users SET fullName=?, login=?, password=?, is_authorized=? WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, user.getFullName());
            stmt.setString(2, user.getLogin());
            stmt.setString(3, user.getPassword());
            stmt.setBoolean(4, true);
            stmt.setInt(5, user.getId());
            stmt.executeUpdate();
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try{
            String sql = "DELETE FROM users.users WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }
}
