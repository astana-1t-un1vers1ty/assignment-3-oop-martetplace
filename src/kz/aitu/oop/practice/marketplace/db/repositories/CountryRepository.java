package kz.aitu.oop.practice.marketplace.db.repositories;

import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.base.Repository;
import kz.aitu.oop.practice.marketplace.models.Country;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class CountryRepository implements Repository<Integer, Country> {
    public static final CountryRepository instance = new CountryRepository();
    private final Connection connection;
    public CountryRepository(){
        this.connection = JDBCConnection.instance.connection;
    }

    @Override
    public Country get(int id) {
        try {
            String sql = "SELECT * FROM refs.countries WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int countryId = rs.getInt("id");
                String countryName = rs.getString("name");
                return new Country(countryId, countryName);
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<Country> getAll() {
        ArrayList<Country> countries = new ArrayList<>();
        try {
            String sql = "SELECT * FROM refs.countries";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int countryId = rs.getInt("id");
                String countryName = rs.getString("name");
                countries.add(new Country(countryId, countryName));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return countries;
    }

    @Override
    public Integer save(Country country) {
        try{
            String sql = "INSERT INTO refs.countries(name) VALUES(?)";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, country.getName());
            int numberOfInsertedRows = stmt.executeUpdate();
            if(numberOfInsertedRows>0){
                try(ResultSet rs = stmt.getGeneratedKeys()){
                    if(rs.next()){
                        return rs.getInt(1);
                    }
                }
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Country country) {
        try{
            String sql = "UPDATE refs.countries SET name=? WHERE id=?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1,country.getName());
            stmt.setInt(2, country.getId());
            stmt.executeUpdate();
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            String sql = "DELETE FROM refs.countries WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }
}
