package kz.aitu.oop.practice.marketplace.db.repositories;

import kz.aitu.oop.practice.marketplace.db.JDBCConnection;
import kz.aitu.oop.practice.marketplace.db.repositories.base.Repository;
import kz.aitu.oop.practice.marketplace.models.City;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class CityRepository implements Repository<Integer, City> {
    public static final CityRepository instance = new CityRepository();
    private final Connection connection;
    public CityRepository(){
        this.connection = JDBCConnection.instance.connection;
    }
    @Override
    public City get(int id) {
        try {
            String sql = "SELECT * FROM refs.cities WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int cityId = rs.getInt("id");
                String cityName = rs.getString("name");
                int regionId = rs.getInt("region_id");
                return new City(cityId, cityName, regionId);
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<City> getAll() {
        ArrayList<City> cities = new ArrayList<>();
        try {
            String sql = "SELECT * FROM refs.cities";
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int cityId = rs.getInt("id");
                String cityName = rs.getString("name");
                int regionId = rs.getInt("region_id");
                cities.add(new City(cityId, cityName, regionId));
            }
        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return cities;
    }

    @Override
    public Integer save(City city) {
        try{
            String sql = "INSERT INTO refs.cities(name, region_id) VALUES(?, ?)";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, city.getName());
            stmt.setInt(2,city.getRegionId());
            int numberOfInsertedRows = stmt.executeUpdate();
            if(numberOfInsertedRows>0){
                try(ResultSet rs = stmt.getGeneratedKeys()){
                    if(rs.next()){
                        return rs.getInt(1);
                    }
                }
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(City city) {
        try{
            String sql = "UPDATE refs.cities SET name=?, region_id=? WHERE id=?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1,city.getName());
            stmt.setInt(2, city.getRegionId());
            stmt.setInt(3, city.getId());
            stmt.executeUpdate();
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            String sql = "DELETE FROM refs.cities WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException throwable){
            throwable.printStackTrace();
        }
    }

    public City[] getByRegionId(int id) {
        ArrayList<City> cities = new ArrayList<>();
        try {
            String sql = "SELECT * FROM refs.cities WHERE id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int cityId = rs.getInt("id");
                String cityName = rs.getString("name");
                int regionId = rs.getInt("region_id");
                cities.add(new City(cityId, cityName, regionId));
            }
        }catch (SQLException throwable){
            throwable.printStackTrace();
        }
        return cities.toArray(new City[0]);
    }
}
