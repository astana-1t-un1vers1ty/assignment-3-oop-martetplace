CREATE SCHEMA refs;

CREATE TABLE refs.countries(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(256)
);

CREATE TABLE refs.regions(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(256),
    country_id INT,
    CONSTRAINT fk_country_region FOREIGN KEY(country_id) REFERENCES refs.countries(id)
);

CREATE TABLE refs.cities(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(256),
    region_id INT,
    CONSTRAINT fk_region_city FOREIGN KEY(region_id) REFERENCES refs.regions(id)
);