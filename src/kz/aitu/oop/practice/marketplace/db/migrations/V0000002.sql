CREATE SCHEMA marketplace;

CREATE TABLE marketplace.categories(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(256) NOT NULL,
    parent_id INT,
    CONSTRAINT fk_categories FOREIGN KEY(parent_id) REFERENCES marketplace.categories(id)
);

CREATE TABLE marketplace.brands(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(256) NOT NULL
);

CREATE TABLE marketplace.products(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(256),
    brand_id INT NOT NULL,
    category_id INT NOT NULL,
    characteristics JSONB,
    CONSTRAINT fk_brand_products FOREIGN KEY(brand_id) REFERENCES marketplace.brands(id),
    CONSTRAINT fk_categories_products FOREIGN KEY(category_id) REFERENCES marketplace.categories(id)
);

CREATE TABLE marketplace.vendors(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(256),
    bin VARCHAR(256)

);

CREATE TABLE marketplace.vendors_products(
    vendor_id INT NOT NULL,
    product_id INT NOT NULL,
    price MONEY NOT NULL,
    CONSTRAINT fk_vendors FOREIGN KEY(vendor_id) REFERENCES marketplace.vendors(id),
    CONSTRAINT fk_products FOREIGN KEY(product_id) REFERENCES marketplace.products(id)
);

CREATE TABLE marketplace.addresses(
    id SERIAL NOT NULL PRIMARY KEY,
    country_id INT NOT NULL,
    region_id INT NOT NULL,
    city_id INT NOT NULL,
    address VARCHAR(256) NOT NULL,
    vendor_id INT NOT NULL,
    CONSTRAINT fk_country_addresses FOREIGN KEY(country_id) REFERENCES refs.countries(id),
    CONSTRAINT fk_region_addresses FOREIGN KEY(region_id) REFERENCES refs.regions(id),
    CONSTRAINT fk_city_addresses FOREIGN KEY(city_id) REFERENCES refs.cities(id),
    CONSTRAINT fk_vendor_addresses FOREIGN KEY(vendor_id) REFERENCES marketplace.vendors(id)
);

