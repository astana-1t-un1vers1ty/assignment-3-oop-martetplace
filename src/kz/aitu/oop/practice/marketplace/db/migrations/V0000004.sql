ALTER TABLE marketplace.vendors DROP COLUMN user_id;

CREATE TABLE marketplace.vendors_users (
    vendor_id INT NOT NULL,
    user_id INT NOT NULL,
    CONSTRAINT fk_vendors_vendors_users FOREIGN KEY(vendor_id) REFERENCES marketplace.vendors(id),
    CONSTRAINT fk_users_vendors_users FOREIGN KEY(user_id) REFERENCES users.users(id)
);
