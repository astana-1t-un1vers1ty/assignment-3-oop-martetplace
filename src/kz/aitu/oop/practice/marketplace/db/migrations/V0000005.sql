CREATE TABLE marketplace.orders(
    id SERIAL NOT NULL PRIMARY KEY,
    product_id INT NOT NULL,
    vendor_id INT NOT NULL,
    address VARCHAR(256) NOT NULL,
    completed BOOLEAN NOT NULL DEFAULT FALSE,
    CONSTRAINT fk_products FOREIGN KEY(product_id) REFERENCES marketplace.products(id),
    CONSTRAINT fk_vendors FOREIGN KEY(vendor_id) REFERENCES marketplace.vendors(id)
);