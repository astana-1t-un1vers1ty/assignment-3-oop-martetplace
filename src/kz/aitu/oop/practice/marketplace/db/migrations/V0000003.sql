CREATE SCHEMA users;

CREATE TABLE users.users(
    id SERIAL NOT NULL PRIMARY KEY,
    fullName VARCHAR(256) NOT NULL,
    login VARCHAR(256) NOT NULL,
    password VARCHAR(256) NOT NULL,
    is_authorized BOOLEAN NOT NULL DEFAULT false
);

ALTER TABLE marketplace.vendors ADD COLUMN user_id INTEGER NOT NULL REFERENCES users.users(id);