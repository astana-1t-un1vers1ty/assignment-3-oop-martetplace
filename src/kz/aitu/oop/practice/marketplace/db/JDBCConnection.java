package kz.aitu.oop.practice.marketplace.db;

import kz.aitu.oop.practice.marketplace.config.Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
    public static final JDBCConnection instance = new JDBCConnection();

    public final Connection connection;

    private JDBCConnection() {
        this.connection = getConnection();
    }

    private Connection getConnection() {
        if (connection != null) {
            return connection;
        }
        Connection con;
        try {
            con = (
                DriverManager.getConnection("jdbc:postgresql://" + Config.properties.getProperty("db_host") + ":" + Config.properties.getProperty("db_port") + "/" + Config.properties.getProperty("db_name"),
                    Config.properties.getProperty("db_username"), Config.properties.getProperty("db_password")));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return con;
    }
}
