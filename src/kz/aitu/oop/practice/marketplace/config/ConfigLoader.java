package kz.aitu.oop.practice.marketplace.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigLoader {
    public static void load() {
        try {
            Properties appSettings = new Properties();
            var basePath = new File("").getAbsolutePath();
            FileInputStream fis = new FileInputStream(basePath + "\\config.properties"); //put config properties file to buffer
            appSettings.load(fis);
            Config.properties = appSettings;
            fis.close();
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }
}
