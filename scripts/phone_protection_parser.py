
import openpyxl as xlsx
import psycopg2 as pg

#CONSTS
sheetName = "Phone protection"
vendors_column = 5
prices_column = 6
c1 = ('Type', 3)
c2 = ('Type of cover', 4)
product_id = 46
to = 50

conn = pg.connect(
  host="localhost",
  database="dbMarketplace",
  user="postgres",
  password="postgres"
)
cur = conn.cursor()

def get_brand_id(brand):
  cur.execute("SELECT id FROM marketplace.brands WHERE name='%s'"%(brand))
  v = cur.fetchall()
  if len(v) > 0:
    v = v[0] 
  else:
    cur.execute("INSERT INTO marketplace.brands(\"name\") VALUES('%s')"%(brand))
    conn.commit()
    cur.execute("SELECT id FROM marketplace.brands WHERE name='%s'"%(brand))
    v = cur.fetchall()[0]
  return v[0]

def get_vendor_id(vendor):
  cur.execute("SELECT id FROM marketplace.vendors WHERE name='%s'"%(vendor))
  v = cur.fetchall()
  if len(v) > 0:
    v = v[0] 
  else:
    cur.execute("INSERT INTO marketplace.vendors(\"name\") VALUES('%s')"%(vendor))
    conn.commit()
    cur.execute("SELECT id FROM marketplace.vendors WHERE name='%s'"%(vendor))
    v = cur.fetchall()[0]
  return v[0]

workbook = xlsx.load_workbook('Electronics.xlsx')
worksheet = workbook[sheetName]

vendors = []

def get_name(name):
  if (name != None and name != ''):
     return name
  
temp = worksheet.cell(row = 2, column = 1).value.strip()
brand = worksheet.cell(row = 2, column = 2).value.strip()
name_c = 2
name = ''

for i in range(2, to):
  if worksheet.cell(row = i, column = 1).value != None and worksheet.cell(row = i, column = 1).value.strip() != temp or worksheet.cell(row = i, column = 1).value == '$':
    # INSERT product, get id
    preparedState = "INSERT INTO marketplace.products(id, \"name\", brand_id, category_id, \"characteristics\") values (%d, '%s', %d, %d, '{\"%s\": \"%s\", \"%s\": \"%s\"}');"%(product_id, name.replace("\"", '').replace("\n", ''), get_brand_id(brand), 5, c1[0], worksheet.cell(row = name_c, column = c1[1]).value.replace("\"", '').replace("\n", ''), c2[0], worksheet.cell(row = name_c, column = c2[1]).value.replace("\"", '').replace("\n", ''))
    cur.execute(preparedState)
    conn.commit()
    for vendor in vendors:
      vendor_id = get_vendor_id(vendor[0])
      cur.execute("INSERT INTO marketplace.vendors_products(vendor_id, product_id, price) VALUES(%d, %d, %d);"%(int(vendor_id), product_id, float(str(vendor[1]).replace(" ", '').replace("\n", ''))))
      conn.commit()
      vendors = []
    product_id += 1
  vendors.append((worksheet.cell(row = i, column = vendors_column).value, worksheet.cell(row = i, column = prices_column).value))
  name = worksheet.cell(row = i, column = 1).value.strip() if worksheet.cell(row = i, column = 1).value != None else name
  if name != temp:
    name_c = i
  brand = worksheet.cell(row = i, column = 2).value.strip() if brand != worksheet.cell(row = i, column = 2).value != None else brand
  temp = name

cur.close()
conn.close()